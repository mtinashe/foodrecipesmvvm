package com.mtinashe.foodrecipes;

import android.view.View;
import android.widget.FrameLayout;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

public abstract class BaseActivity extends AppCompatActivity {

    public ProgressBar mProgressbar;

    @Override
    public void setContentView(int layoutResID) {
        ConstraintLayout cLayout = (ConstraintLayout) getLayoutInflater().inflate(R.layout.activity_base, null);
        FrameLayout frameLayout = cLayout.findViewById(R.id.activity_content);
        mProgressbar = cLayout.findViewById(R.id.m_progress_bar);

        getLayoutInflater().inflate(layoutResID, frameLayout,true);

        super.setContentView(cLayout);
    }

    public void showProgressBar(boolean visibility){
        mProgressbar.setVisibility(visibility ? View.VISIBLE : View.INVISIBLE);
    }
}
