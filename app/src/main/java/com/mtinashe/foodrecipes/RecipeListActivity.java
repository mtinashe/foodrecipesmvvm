package com.mtinashe.foodrecipes;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class RecipeListActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe_list);

        findViewById(R.id.btnStartStop).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mProgressbar.getVisibility() == View.VISIBLE){
                    showProgressBar(false);
                } else {
                    showProgressBar(true);
                }
            }
        });
    }
}
